import django
import os
import sys
import time
import json
import requests


sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '..', '..')))
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "hats_project.settings")
django.setup()


from hats_rest.models import Hat


def get_hats():
    response = requests.get('http://wardrobe-api:8000/api/locations/')
    content = json.loads(response.content)
    for hat in content['hats']:
        Hat.objects.create(
            style_name=hat['style_name'],
            color=hat['color'],
            fabric=hat['fabric'],
            image_url=hat['image_url'],
            location=hat['location']
        )


def poll():
    while True:
        print('Hats poller polling for data')
        try:
            get_hats()
        except Exception as e:
            print(e, file=sys.stderr)
        time.sleep(60)


if __name__ == "__main__":
    poll()
