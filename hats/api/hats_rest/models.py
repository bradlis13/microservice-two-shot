from django.db import models
from django.urls import reverse
from django.core.exceptions import ValidationError


class Hat(models.Model):
    style_name = models.CharField(max_length=100)
    color = models.CharField(max_length=100)
    fabric = models.CharField(max_length=100)
    image_url = models.URLField(blank=False)
    location = models.CharField(max_length=200)

    def get_api_url(self):
        return reverse("api_list_hats", kwargs={"hat_id": self.pk})

    def __str__(self):
        return f"{self.style_name} - {self.fabric} - at {self.location}"

    def clean(self):

        if not self.style_name:
            raise ValidationError("Style name is required.")
        if not self.color:
            raise ValidationError("Color is required.")
        if not self.fabric:
            raise ValidationError("Fabric is required.")
        if not self.image_url:
            raise ValidationError("Image URL is required.")
        if not self.location:
            raise ValidationError("Location is required.")
