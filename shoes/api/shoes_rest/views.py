from django.http import JsonResponse
from django.shortcuts import render
from common.json import ModelEncoder
from .models import Shoe, BinVO
from django.views.decorators.http import require_http_methods
import json
# import sys

# Create your views here.
class BinVOEncoder(ModelEncoder):
    model = BinVO
    properties = ["closet_name","import_href", "id"]

class ShoeListEncoder(ModelEncoder):
    model = Shoe
    # properties = ["name","id","manufacturer","color","image"]
    properties = [
        "name",
        "manufacturer",
        "color",
        "picture_url",
        "bin",
        "id",
        
        ]
    encoders= {
        "bin": BinVOEncoder(),
    }

    def get_extra_data(self, o):
        return {"bin": o.bin.closet_name}

class ShoeDetailEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "id",
        "manufacturer",
        "name",
        "color",
        "picture_url",
        "bin",
    ]
    encoders= {
        "bin": BinVOEncoder(),
    }
    

@require_http_methods(["GET", "POST"])
def api_list_shoes(request):
    if request.method == "GET":
        shoes = Shoe.objects.all()
        return JsonResponse(
            {"shoes":shoes},
            encoder=ShoeDetailEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            bin_href = content["bin"]
            bin = BinVO.objects.get(import_href=bin_href)
            content["bin"]=bin
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message":"Invalid bin id"},
                status=400,
            )
        shoes = Shoe.objects.create(**content)
        return JsonResponse(
            shoes,
            encoder=ShoeDetailEncoder,
            safe=False,
        )

@require_http_methods(["DELETE","GET","PUT"])
def api_shoes(request,pk):
    if request.method == "GET":
        try:
            shoe = Shoe.objects.get(id=pk)
            return JsonResponse(
                shoe,
                encoder= ShoeDetailEncoder,
                safe=False
            )
        except Shoe.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
    elif request.method=="DELETE":
        try:
            shoe = Shoe.objects.get(id=pk)
            shoe.delete()
            return JsonResponse(
                shoe,
                encoder= ShoeDetailEncoder,
                safe=False,
            )
        except Shoe.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
    else:
        try:
            content = json.loads(request.body)
            shoe = Shoe.objects.get(id=pk)
            
            props = ["manufacturer","name","color","image","bin"]
            for prop in props:
                if prop in content:
                    setattr(shoe, prop, content[prop])
            shoe.save()
            return JsonResponse(shoe, encoder=ShoeDetailEncoder,safe=False,)
        except Shoe.DoesNotExist:
            response = JsonResponse({"message":"Does not exist"})
            response.status_code = 404
            return response
