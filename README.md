# Wardrobify

Team:

* Person 1 - Hung Hoang - Shoes
* Person 2 - Bradley Belcher - Hats

## Design

## Shoes microservice
Hung:
Approach:
    Backend:
        I started off by building the django backend, creating BinVO and other models/views, and encoders for the views. Also set up polling for the shoes api. Set up the urlpatterns for the shoes backend. Verified that my code worked by using insomnia to test out sending JSON bodies using GET, POST requests.

            Main files include: shoes_rest(models.py, admin.py, models.py, urls.py, views.py)
    Frontend:
        Built out the React frontend. ShoesList and ShoesForm was created with various promises and fetch requests to get the data from the backend database to populate into the frontend. Then I linked ShoesList.js, ShoeForm.js, and Nav.js to populate on App.js .
        Copious amounts of useEffect, usestate, fetch requests, and array mapping used.
            Main files include: ghi/app/src/(App.js, Nav.js, ShoeForm.js, ShoeList.js)
Explain your models and integration with the wardrobe
microservice, here.

## Hats microservice


Belcher:
    Approach:
        I took the same approach as my partner. I built the backend code first, creating all the necessary code for models, views, admin, etc, with a focus on API's. The difficulty I faced was trying to have the different projects talk to eachother without using serialization.  I tried to do it that way until an error code showed me the code written into this project wouldn't allow it. I wanted to build the backend first, as that was what I was most used to, I had issues with writing code that would utilize locations created in the wardrobe app.  My partner and I took different approaches to this problem, as we worked separately over the weekend. I was able to create locations within insomnia from the wardrobe app but due to errors and the design of the app the two separate projects of hats and wardrobe couldn't speak on the back end. I desigend as much code as I could then moved to the front end.

        on the front end I created two .js files, one to list hats and one to create hats. I also created a .css file to handle all the specifics of my table in the .js files rather than spend the time incorporating all the individual table components into each line of the html for spacing and sizing.

        the biggest difference we noticed between our approaches was how I ended up refreshing the page after creating and deleting items, and how we retrieved infromation from the wardrobe app. They both seem effective but my partners appears more efficient. I also had to write code on the front end that allowed my hats project to communicate with the wardrobe project and retrieve the necessary wardrobe information and input it into creating my hats and place on the tables. I recieved numerous error codes during this process and the most effective solution I found was to alter the wardrobe application code slightly to include permissions. I think there is a more efficient way of doing this but in the interest of not spending 280 hours redoing all my code, I left the functioning code as is, becuase it worked so I wans't going to mess with it further.